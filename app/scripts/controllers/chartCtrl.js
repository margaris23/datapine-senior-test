'use strict';

/**
 * @module datapineTest.controllers
 **/
controllers.controller('ChartCtrl', [
    '$scope',
    'ChartSvc',
    '$stateParams',
    ChartCtrl]);

/**
 *
 * @function ChartCtrl
 * @description Controller which handles charts list
 * @return {controller}
 * @param {$scope} $scope
 * @param {service} ChartSvc
 * @param {ui-route-params} $stateParams
 *
 **/
function ChartCtrl($scope, ChartSvc, $stateParams) {
        console.log('ChartCtrl started!');

        // initialization
        ChartSvc.get($stateParams.id, function (error, chart) {
            if (error || !chart) {
                console.log('chart not found!');
                return;
            }

            $scope.chart = chart;

	    // Make sure Highcharts is loaded when controller needs it
            var options = chart;
            options.chart.renderTo = 'container';
            new Highcharts.Chart(options);
	});

        $scope.$on("$destroy", function () {
            console.log('ChartCtrl destroyed!');
        });
}
