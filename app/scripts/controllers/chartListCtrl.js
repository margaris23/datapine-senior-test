'use strict';

/**
 * @module datapineTest.controllers
 **/
var controllers = angular.module('datapineTest.controllers', []);

controllers.controller('ChartListCtrl', [
    '$scope',
    '$timeout',
    'ChartSvc',
    'holderjs',
    ChartListCtrl]);

/**
 *
 * @function ChartListCtrl
 * @description Handles list of charts
 * @return {controller}
 * @param {$scope} $scope
 * @param {$timeout} $timeout
 * @param {service} ChartSvc
 * @param {3rd-party-lib} holderjs
 *
 **/
function ChartListCtrl($scope, $timeout, ChartSvc, holderjs) {
        console.log('ChartListCtrl started!');

        // initialization
	$scope.loading = true;
	
	ChartSvc.list(function (error, charts) {

	    // start a timer to change loading status in case
	    // it takes too long
	    $timeout(function () {
		$scope.loading = false;
	    }, 5000);
	    
	    if (error || !charts) {
                $scope.charts = [];
		console.log('Error or no data to display!');
                return;
            }

            $timeout(function () {
		 $scope.charts = charts;
	    });
        });

	// Listeners
        $scope.$on("$destroy", function () {
            console.log('ChartListCtrl destroyed!');
        });

	$scope.$on('charts-done', function () {
	    $scope.loading = false;
	});

	// Used for generating holder images
        $scope.holderjs = holderjs;
}
