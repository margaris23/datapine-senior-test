/*global require*/
'use strict';

require.config({
    shim: {
        highcharts: {
            deps: [
                'jquery'
            ]
        },
        angular: {
            deps: [
                'highcharts',
                'jquery'
            ],
            exports: 'angular'
        },
        uirouter: {
            deps: [
                'angular'
            ],
            exports: 'uirouter'
        },
        directives: {
            deps: [
                'angular',
                'services'
            ],
            exports: 'directives'
        },
        controllers: {
            deps: [
                'angular',
                'uirouter'
            ]
        },
        chartCtrl: {
            deps: [
                'controllers'
            ],
            exports: 'chartCtrl'
        },
        services: {
            deps: [
                'angular'
            ]
        }
    },
    paths: {
        jquery: '../bower_components/jquery/jquery',
        angular: '../bower_components/angular/angular',
        uirouter: '../bower_components/angular-ui-router/release/angular-ui-router',
        directives: 'directives/directives',
        controllers: 'controllers/chartListCtrl',
        chartCtrl: 'controllers/chartCtrl',
        services: 'services/chartSvc',
        highcharts: '../bower_components/highcharts-release/highcharts',
        holderjs: '../bower_components/holderjs/holder'
    }
});

require([
    'holderjs',
    'highcharts',
    'angular',
    'uirouter',
    'services',
    'directives',
    'controllers',
    'chartCtrl'
], function (Holder, highcharts, angular) {

    // Thirdparty modules
    angular.module('datapineTest.thirdparty', [])
    .factory('holderjs', function () {
        return Holder;
    });

    // Main app
    angular.module('datapineTest', [
        'ui.router',
        'datapineTest.thirdparty',
        'datapineTest.services',
        'datapineTest.directives',
        'datapineTest.controllers'
    ]).config(function ($stateProvider, $urlRouterProvider) {

        // Routing
        $urlRouterProvider.when('', '/charts/list');
        $urlRouterProvider.when('/', '/charts/list');

        // For any unmatched url, send to /charts/list
        $urlRouterProvider.otherwise('/charts/list');

        $stateProvider
            .state('charts', {
                abstract: true,
                url: '/charts',
                template: '<div ui-view></div>'
            })
            .state('charts.list', {
                url: '/list',
                // loaded into ui-view of parent's template
                templateUrl: 'partials/charts.html',
                controller: 'ChartListCtrl'
            })
            .state('charts.detail', {
                url: '/:id',
                // loaded into ui-view of parent's template
                templateUrl: 'partials/chart.html',
                controller: 'ChartCtrl'
            }).state('about', {
              url: '/about',
              templateUrl: 'partials/help.html'
            });
    });

    // manual angular bootstrapping
    angular.bootstrap(document, ['datapineTest']);
});
