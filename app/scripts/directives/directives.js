/**
 * @module datapineTest.directives
 **/
angular.module('datapineTest.directives', [])
    .directive('chartItem', ChartItem);

/**
 *
 * @function ChartItem
 * @description Directive showing injection of 3rdparty libs
 * @return {directive}
 **/
function ChartItem() {
    return {
        restrict: 'E',
        scope: {
            holderjs: '=', // 3rd party lib
	    title: '@',
	    isLast: '='
        },
        link: function (scope, element) {
	    scope.holderjs.addImage('holder.js/200x200?random=yes&text=' +
				    scope.title, element[0]);
	    scope.holderjs.run();

	    // notify controllers when last item finished loading
	    if (scope.isLast) {
		scope.$emit('charts-done');
	    }
        }
    };
}
