/**
 * @module datapine.services
 **/
angular.module('datapineTest.services', [])
.factory('ChartSvc', [
    ChartSvc
]);

/**
 *
 * @function ChartSvc
 * @description Chart Service which generates charts for demonstration purposes
 * @return {{list: function, get: function}}
 *
 **/ 
function ChartSvc() {
    console.log('ChartSvc started!');

    var CHART_TYPES = Object.freeze(['bar', 'line', 'column', 'spline', 'pie', 'scatter']);
	
    /**
     * @function Chart
     * @description Chart generator
     * @return {Chart}
     * @param {CHART_TYPE} chartType type of chart
     **/

    var Chart = function (chartType) {
	return {
            chart: {
                type: chartType
            },
            title: {
                text: 'Fruit Consumption_' + chartType 
            },
            xAxis: {
                categories: ['Apples',
			     'Bananas',
			     'Oranges',
			     'Melons',
			     'Grappes',
			     'Kiwis',
			     'Blue berries',
			     'Strawberries']
            },
            yAxis: {
                title: {
                    text: 'Fruit eaten'
                }
            },
            series: [{
                name: 'Nikos',
                data: [1, 0, 4, 3, 2, 4, 1, 6]
            }, {
                name: 'Vaya',
                data: [5, 7, 3, 6, 3, 5, 3, 7]
            }]
        };
    };

    // Initialization of charts
    var _charts = [];
    for (var i=0; i<30; i++) {
	var index = Math.floor((Math.random() * CHART_TYPES.length - 1) + 1);
	_charts.push(Chart(CHART_TYPES[index]));
    }

    return {
	/**
	 * @function list
         * @description Lists charts
         **/
        list: function (cb) {
            cb = cb  || function () {};

	    // Charts are generated for demonstration purposes
            cb(null, _charts);
        },
	/**
         * @function get
         * @description Gets a chart fromn list
         * @return {Chart}
         * @param {integer} id index of chart in list
         **/
        get: function (id, cb) {
            cb = cb  || function () {};

            // validation
            if (!id || id < 0 || id > _charts.length - 1) {
                return cb('Wrong index!');
            }

            cb(null, _charts[id]);
        }
    };
}
